import os
from os.path import dirname, abspath
from dotenv import load_dotenv

load_dotenv()


class Config:
    DEBUG = False
    DEVELOPMENT = False
    SECRET_KEY = os.environ.get('FLASK_SECRET_KEY') or 'some_secret_key_name'
    SECURITY_PASSWORD_SALT = os.environ.get('SECURITY_PASSWORD_SALT') or SECRET_KEY
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'true').lower() in ['true', 'on', '1']
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    APP_MAIL_SUBJECT_PREFIX = '[APP]'
    MAIL_DEFAULT_SENDER = 'testapp.ilya@gmail.com'
    APP_ADMIN = os.environ.get('APP_ADMIN')
    STATIC_PATH = os.path.join(dirname(dirname(abspath(__file__))), f'static{os.sep}')
    MAX_CONTENT_LENGTH = 4 * 1024 * 1024
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_aoo(self):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    DEVELOPMENT = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://' + os.getenv('DB_USERNAME') + ':' + os.getenv('DB_PASSWORD') + \
                              '@' + os.getenv('DB_HOST') + ':' + os.getenv('DB_PORT') + '/' + os.getenv('DEV_DB_NAME')


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://' + os.getenv('DB_USERNAME') + ':' + os.getenv('DB_PASSWORD') + \
                              '@' + os.getenv('DB_HOST') + ':' + os.getenv('DB_PORT') + '/' + os.getenv('DEV_DB_NAME')
