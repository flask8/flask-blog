from flask_wtf import FlaskForm
from wtforms import PasswordField, SubmitField, BooleanField, StringField, EmailField, ValidationError
from wtforms.validators import DataRequired, Length, EqualTo, Regexp, Email
from app.models import Users


class LoginForm(FlaskForm):
    email = EmailField('Enter email', validators=[DataRequired(), Length(1, 64), Email()])
    password = PasswordField('Enter password', validators=[DataRequired()])
    remember = BooleanField('Keep me logging in')
    submit = SubmitField('Login')


class RegisterForm(FlaskForm):
    email = EmailField('Enter email', validators=[DataRequired(), Length(1, 64), Email()])
    name = StringField('Enter your username', validators=[DataRequired(), Length(1, 64),
                                                          Regexp('^[A-Za-z][A-Za-z0-9_]*$', 0, 'Username must have '
                                                                                               'only letters, numbers '
                                                                                               'or underscores')])
    password = PasswordField('Enter password', validators=[DataRequired(), Length(min=6, max=35)])
    password_confirm = PasswordField('Confirm password', validators=[DataRequired(), Length(min=6, max=35),
                                     EqualTo('password', message='Both password fields must be equal')])
    submit = SubmitField('Register')

    def validate_email(self, field):
        if Users.query.filter_by(email=field.data).first():
            raise ValidationError('Email already exists!')

    def validate_name(self, field):
        if Users.query.filter_by(name=field.data).first():
            raise ValidationError('Username already in use')


class ForgotPasswordForm(FlaskForm):
    email = EmailField('Enter your email', validators=[DataRequired(), Length(1, 64), Email()])
    submit = SubmitField('Send')


class ChangeForgottenPasswordForm(FlaskForm):
    password = PasswordField('Enter new password', validators=[DataRequired(), Length(6, 35)])
    password_confirm = PasswordField('Confirm new password',
                                     validators=[DataRequired(), EqualTo('password', message='oth password fields '
                                                                                             'must be equal!')])
    submit = SubmitField('Change')

