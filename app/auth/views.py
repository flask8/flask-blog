from flask import redirect, render_template, url_for, flash, Markup
from flask_login import current_user, logout_user, login_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from app import db
from . import auth
from app.models import Users, UserInfo
from .forms import LoginForm, RegisterForm, ChangeForgottenPasswordForm, ForgotPasswordForm
from app.scripts.confirm_email import generate_confirmation_token, confirm_token, send_email, send_confirm_letter, \
    send_password_change_btn
from app.blog.forms import SearchForm
from app.scripts.main_func import add_user_event, get_user_location


@auth.route('/login', methods=['POST', 'GET'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = db.session.query(Users).filter_by(email=form.email.data).first()
        if not user or not user.verify_password(form.password.data):
            flash('Incorrect password or user doesn\'t exists', 'warning')
            return render_template('auth/login_form.html', login_form=form)
        if login_user(user, remember=bool(form.remember.data)):
            add_user_event('Log in', get_user_location(), current_user)
        return redirect(url_for('blog.index'))
    return render_template('auth/login_form.html', login_form=form)


@auth.route('/register', methods=['POST', 'GET'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        user = Users(email=form.email.data,
                     name=form.name.data)
        user.set_password(form.password.data)
        user_info = UserInfo(user=user)
        db.session.add(user)
        db.session.add(user_info)
        db.session.commit()
        send_confirm_letter(form.email.data)
        if login_user(user, remember=True):
            add_user_event('Account created', get_user_location(), current_user)
            add_user_event('Log in', get_user_location(), current_user)
        flash('Please confirm your email', 'warning')
        return redirect(url_for('blog.index'))
    return render_template('auth/register_form.html', form=form)


@auth.route('/confirm/<token>')
@login_required
def confirm_email(token):
    email = ''
    try:
        email = confirm_token(token)
    except:
        flash('The confiration link broken or has expired', 'error')
    if not email:
        return redirect(url_for('auth.unconfirmed'))
    user = db.session.query(Users).filter_by(email=email).first_or_404()
    if user:
        u = db.session.query(UserInfo).filter_by(user_id=user.id).first()
        if u.confirmed:
            flash('Account already confirmed!', 'success')
        else:
            u.confirmed = True
            db.session.add(u)
            db.session.commit()
            flash('You have confirmed your account. Thanks!', 'success')
    return redirect(url_for('blog.index'))


@auth.route('/unconfirmed')
@login_required
def unconfirmed():
    if current_user.info.confirmed:
        return redirect(url_for('blog.index'))
    flash('Please confirm your email!', 'warning')
    return render_template('auth/unconfirmed.html')


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('blog.index'))


@auth.route('/resend')
@login_required
def resend():
    subject = 'Email was sent again. Please confirm your email!'
    send_confirm_letter(current_user.email, subject)
    flash(subject, 'success')
    return redirect(url_for('auth.unconfirmed'))


@auth.context_processor
def base():
    form = SearchForm()
    return dict(form=form)


@auth.route('/password_recovery', methods=['GET', 'POST'])
def password_recovery():
    form = ForgotPasswordForm()
    if form.validate_on_submit():
        if db.session.query(Users).filter_by(email=form.email.data).first() is not None:
            send_password_change_btn(form.email.data)
            flash(Markup(f'Email with instructions was sent on your mail: <b>{form.email.data}</b>'), 'success')
            return redirect(url_for('auth.login'))
        else:
            flash('There is no user with this email', 'error')
    return render_template('auth/password_recovery.html', recovery_form=form)


@auth.route('/change_password/<token>', methods=['GET', 'POST'])
def password_change(token):
    form = ChangeForgottenPasswordForm()
    email = ''
    try:
        email = confirm_token(token)
    except:
        flash('The confirmation link broken or has expired', 'error')
    if not email:
        flash('Something went wrong! Please try this operation later', 'error')
        return redirect(url_for('blog.index'))

    if form.validate_on_submit():
        if db.session.query(Users).filter_by(email=email).first() is not None:
            user = db.session.query(Users).filter_by(email=email).first()
            user.set_password(form.password.data)
            db.session.commit()
            flash('Password was changed!', 'success')
            return redirect(url_for('blog.index'))

    if email:
        return render_template('auth/change_password.html', chg_psw_form=form)
    return redirect(url_for('blog.index'))

