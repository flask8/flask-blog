from flask_admin.contrib.sqla import ModelView
from flask_admin import AdminIndexView
from flask_login import current_user
from flask import redirect, url_for, flash


class CustomAdminIndexView(AdminIndexView):
    def is_accessible(self):
        if current_user.is_authenticated and not current_user.is_anonymous:
            return current_user.info.admin_rights
        return False

    def inaccessible_callback(self, name, **kwargs):
        flash('You have no permissions!', 'error')
        return redirect(url_for('blog.index'))


class BaseModelView(ModelView):
    page_size = 40

    def is_accessible(self):
        if current_user.is_authenticated and not current_user.is_anonymous:
            return current_user.info.admin_rights
        return False

    def inaccessible_callback(self, name, **kwargs):
        flash('You have no permissions!', 'error')
        return redirect(url_for('blog.index'))


class UsersModelView(BaseModelView):
    column_searchable_list = ['email', 'name']
    column_filters = ['info.confirmed', 'info.premium', 'info.admin_rights']
    column_list = ['id', 'email', 'name', 'info.articles_count', 'info.premium']


class UsersInfoModelView(BaseModelView):
    column_searchable_list = ['user.name', 'user.email']
    column_filters = ['confirmed', 'premium', 'admin_rights']
    column_list = ['id', 'user.name', 'confirmed', 'premium', 'premium_expired', 'admin_rights']


class TagsModelView(BaseModelView):
    column_searchable_list = ['tag_name']
    column_list = ['id', 'tag_name']


class ArticlesModelView(BaseModelView):
    column_searchable_list = ['user.name', 'title', 'article_body']
    column_list = ['id', 'title', 'article_body', 'comments']


class UserEventModelView(BaseModelView):
    column_searchable_list = ['user.name', 'user.id']
    column_filters = ['event_title']
    page_size = 100


class CommentsModelView(BaseModelView):
    column_searchable_list = ['user.name', 'comment_text']
    column_filters = ['user.name']
