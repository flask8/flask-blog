from app import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


class Base(db.Model):
    __abstract__ = True
    on_update = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())
    on_create = db.Column(db.DateTime, default=db.func.now())


class Users(UserMixin, Base):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), nullable=False, unique=True)
    password_hash = db.Column(db.Text, nullable=False)
    name = db.Column(db.String(120), nullable=False)
    info = db.relationship('UserInfo', backref='user', lazy=True, uselist=False, cascade='all,delete')
    articles = db.relationship('Articles', backref='user', lazy=True, cascade='all,delete')
    comments = db.relationship('Comments', backref='user', lazy=True, cascade='all,delete')
    user_events = db.relationship('UserEvent', backref='user', lazy=True, cascade='all,delete')
    votes = db.relationship('PostRating', backref='user', lazy=True, cascade='all, delete')

    def __init__(self, email, name):
        self.email = email
        self.name = name

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return f'<User {self.name}>'


class UserInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    confirmed = db.Column(db.Boolean, default=False)
    premium = db.Column(db.Boolean, default=False)
    premium_expired = db.Column(db.DateTime)
    admin_rights = db.Column(db.Boolean, default=False)
    articles_count = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)


ArticleTag = db.Table('article_tag',
                      db.Column('articles_id', db.Integer, db.ForeignKey('articles.id')),
                      db.Column('tags_id', db.Integer, db.ForeignKey('tags.id'))
                      )


class Articles(Base):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, nullable=False)
    article_body = db.Column(db.Text, nullable=False)
    author = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    tags = db.relationship('Tags', secondary=ArticleTag, backref='article')
    comments = db.relationship('Comments', backref='article', lazy=True, cascade='all,delete')
    rating = db.relationship('PostRating', backref='article', lazy=True, cascade='all, delete')

    def __init__(self, title, article_body, author):
        self.title = title
        self.article_body = article_body
        self.author = author

    def __repr__(self):
        return f'<Articles {self.title}>'


class Tags(Base):
    id = db.Column(db.Integer, primary_key=True)
    tag_name = db.Column(db.String(100))

    def __repr__(self):
        return f'<Tags {self.tag_name}>'


class Comments(Base):
    id = db.Column(db.Integer, primary_key=True)
    comment_text = db.Column(db.String(300))
    author = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey('articles.id'), nullable=False)

    def __init__(self, comment_text, author, post_id):
        self.comment_text = comment_text
        self.author = author
        self.post_id = post_id

    def __repr__(self):
        return f'<Comments {self.author} : {self.post_id}>'


class UserEvent(Base):
    id = db.Column(db.Integer, primary_key=True)
    event_title = db.Column(db.String(200), nullable=False)
    event_desc = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    def __init__(self, event_title, event_desc):
        self.event_title = event_title
        self.event_desc = event_desc

    def __repr__(self):
        return f'<UserEvent {self.event_title}>'


class PostRating(Base):
    id = db.Column(db.BigInteger, primary_key=True)
    post_id = db.Column(db.Integer, db.ForeignKey('articles.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    rating = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f"<PostRating (id='{self.id}, " \
               f" rating='{self.rating}', " \
               f" user_id='{self.user_id}', " \
               f"post_id='{self.post_id}'>"
