$(document).ready(function () {
      $("[id^='btn-delete-']" ).click(function( event ) {
        count_articles_on_page();
        let id = event.target.id;
        let id_start_pos = id.lastIndexOf('-');
        let post_id = id.slice(id_start_pos+1);
        deletePost( post_id);
        event.preventDefault();
      });
  });


function deletePost(id) {
    let form = $('#delete-form-' + id);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (response) {
            $("div#article-"+id).remove();

            },
            error: function (error) {
                console.log(error);
            }
        });
    }


function count_articles_on_page(){
        let items = $("[id^='btn-delete-']").length - 1;
        let url = window.location.href;
        let page_pos= url.lastIndexOf('/');
        let index_pos = url.lastIndexOf('index');
        if (page_pos >= (index_pos+5)){
            let page = parseInt(url.slice(page_pos + 1));
            let updated_url = url.slice(0, page_pos + 1);
            if (page > 1 && items === 0){
              window.location.replace(updated_url + (page - 1));
            }
        }
    }