$(document).ready(function () {
    $("[id^='vote-btn-']").click( function (e){
        let btn_id = $(this).attr('id').split('-', 4).pop();
        let form_status = $(this).attr('value');
        rmActiveClass(btn_id);
        $(this).addClass('disabled')
        vote(btn_id, form_status);
        e.preventDefault();
    });
});

function vote(id, status) {
    let s = id + '|' + status;
    $.ajax({
        type: 'POST',
        url: '/vote',
        data: { 'data': s },
        success: function (response) {
            let show_rating = $('#align-rating-' + id);
            show_rating.empty();
            show_rating.append(response.rating);
            },
        error: function (error) {
            console.log(error);
        }
    });
}

function rmActiveClass(id){
        $("#vote-btn-up-" + id).removeClass('disabled');
        $("#vote-btn-down-" + id).removeClass('disabled');

}

