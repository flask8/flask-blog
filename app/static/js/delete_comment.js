  $(document).ready(function () {
      $("[id^='btn-delete-']" ).click(function( event ) {
        let id = event.target.id;
        let id_start_pos = id.lastIndexOf('-');
        let article_id = id.slice(id_start_pos + 1);
        deleteComment( article_id);
        event.preventDefault();
      });
  });


function deleteComment(id) {
    let form = $('#delete-comment-form-' + id);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (response) {
            $("div#cm-block-"+id).remove();
            if (response.status === 'success'){
                console.log(response.msg)
                spawnTempMsgComment(id, response.msg, response.status)
            }else if( response.status==='error'){
                console.log('err')
            }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }


function spawnTempMsgComment(id, data, status) {
    let alert_msg = '';
    if (status ==='success'){
       alert_msg = $("<div class=\"alert alert-success d-flex align-items-center\" role=\"alert\">\n" +
        "  <svg class=\"bi flex-shrink-0 me-2\" width=\"24\" height=\"20\" role=\"img\" aria-label=\"Success:\">" +
        "<use xlink:href=\"#check-circle-fill\"/></svg>\n" +
        "  <div>\n" +
        data +
        "  </div>\n" +
        "</div>").appendTo("#comment-status");
    }else{
        let alert_msg = $("<div class=\"alert alert-danger d-flex align-items-center\" role=\"alert\">\n" +
        "  <svg class=\"bi flex-shrink-0 me-2\" width=\"24\" height=\"20\" role=\"img\" aria-label=\"Success:\">" +
        "<use xlink:href=\"#check-circle-fill\"/></svg>\n" +
        "  <div>\n" +
        "    " + data + "\n" +
        "  </div>\n" +
        "</div>").appendTo("#comment-status");
    }
  setTimeout(function() {
    alert_msg.remove();
  }, 2800);
}