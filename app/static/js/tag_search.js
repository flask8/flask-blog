$(function() {
    $("#tag-search").autocomplete({
        source:function(request, response) {
            $.getJSON("/tag_search",{
                q: request.term,
            }, function(data) {
                if (data.matching_results.length === 0){
                    let a = [request.term + ' (create new tag)']
                    response(a);
                }else{
                     response(data.matching_results);
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            let val = ui.item.value;
            let pos = val.lastIndexOf(' (create new tag)')
            let res ='';
            if (pos !== -1){
                 res = val.substring(0,pos);
            }else{
                res = val;
            }
            if (res.indexOf(',') !== -1){
                let tags_in_string = split_tags(res);
                for (let i=0; i < tags_in_string.length; i++){
                    $('#tags').append('<span class="badge bg-dark badge-space" onclick=\'remove(this)\'>' + tags_in_string[i] + '</span>');
                }
            }else{
                //console.log('HERE: '+ res);
                $('#tags').append('<span class="badge bg-dark badge-space" onclick=\'remove(this)\'>' + res + '</span>');
            }
            ui.item.value='';
        }
    });
})

function remove(el){
    el.remove();
}

function split_tags(tags_str){
    let tags_arr = tags_str.split(',');
    let clear_arr = [];
    for (let i=0; i < tags_arr.length; i++){
        clear_arr.push(tags_arr[i].trim());
    }
    return clear_arr;
}

function pack_tags(){
    let tags=[];
    $('#tags').children('span').each(function(){
        tags.push($(this).text());
    });
    $('#tags-hidden').val([...new Set(tags)])
}