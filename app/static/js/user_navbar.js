$(document).ready(function () {
        $('.user-nav').removeClass('active');
        let href = window.location.href;
        let page = href.substring(href.lastIndexOf('/') + 1);
        let id = page.length !== 0 ? '#user-' + page : '#user-profile';
        $(id).addClass('active');
});