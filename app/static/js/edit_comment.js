  $(document).ready(function () {
      $("[id^='cm-id-']" ).click(function( event ) {
        let id = event.target.id;
        let id_start_pos = id.lastIndexOf('-');
        let comment_id = id.slice(id_start_pos+1);
        updateComment( comment_id);
        event.preventDefault();
      });
  });


function updateComment(id) {
    let form = $('#edit-comment-' + id);
    let new_comment = pack_comment(id, form);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (response) {
            let comment_card = $('#cm-block-'+id);
            $(comment_card).find('div.comment-text').html(new_comment);
            $(comment_card).remove();
            $('#comment-card').append(comment_card);
            if (response.msg !== 'success'){
                spawnTempMsg(id, true,  response.msg)
            }else{
                spawnTempMsg(id, false)
            }

        },
        error: function (error) {
            console.log(error);
        }
    });
}

function pack_comment(id, form){
    let iframe = document.getElementById("edit-comment-" + id).querySelector('iframe');
    let iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
    let body = $(iframeDocument.querySelector('body')).html();
    let  hidden_field = $(form).find('#hidden_com_id');
    if ((hidden_field.val()).indexOf('|') !== -1){
        hidden_field.val(id);
    }
    hidden_field.val(hidden_field.val()+ '|' + body);
    return body;
}


function spawnTempMsg(id, err_status, resp=null) {
    let alert_msg = ''
    if (err_status === true){
        alert_msg = $("<div class=\"alert alert-danger d-flex align-items-center\" role=\"alert\">\n" +
        "  <svg class=\"bi flex-shrink-0 me-2\" width=\"24\" height=\"20\" role=\"img\" aria-label=\"Success:\">" +
        "<use xlink:href=\"#check-circle-fill\"/></svg>\n" +
        "  <div>\n" +
        "    " + resp + "\n" +
        "  </div>\n" +
        "</div>").appendTo("#ajax-message-" + id);
    }else{
        alert_msg = $("<div class=\"alert alert-success d-flex align-items-center\" role=\"alert\">\n" +
        "  <svg class=\"bi flex-shrink-0 me-2\" width=\"24\" height=\"20\" role=\"img\" aria-label=\"Success:\">" +
        "<use xlink:href=\"#check-circle-fill\"/></svg>\n" +
        "  <div>\n" +
        "    Updated\n" +
        "  </div>\n" +
        "</div>").appendTo("#ajax-message-" + id);
    }

  setTimeout(function() {
    alert_msg.remove();
  }, 3500);
}