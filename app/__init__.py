import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bootstrap import Bootstrap5
from flask_mail import Mail
from flask_ckeditor import CKEditor
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from app.admin_model_views import CustomAdminIndexView, UsersModelView, UsersInfoModelView, ArticlesModelView, \
    TagsModelView, UserEventModelView, CommentsModelView


db = SQLAlchemy()
mail = Mail()
bootstrap = Bootstrap5()
ckeditor = CKEditor()
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
admin = Admin(name='microblog', template_mode='bootstrap3', index_view=CustomAdminIndexView())


def create_app():
    app = Flask(__name__)
    app.config.from_object(os.environ.get('APP_SETTINGS'))

    register_blueprints(app)
    initialize_extensions(app)
    add_admin_views()

    from .models import Users

    @login_manager.user_loader
    def load_user(user_id):
        return Users.query.get(int(user_id))
    with app.app_context():
        db.create_all()
    return app


def register_blueprints(app):
    from .auth import auth
    app.register_blueprint(auth)
    from .blog import blog
    app.register_blueprint(blog)
    from .comment import comment
    app.register_blueprint(comment)


def initialize_extensions(app):
    db.init_app(app)
    bootstrap.init_app(app)
    mail.init_app(app)
    ckeditor.init_app(app)
    admin.init_app(app)
    login_manager.init_app(app)


def add_admin_views():
    from .models import Users, UserInfo, UserEvent, Articles, Tags, Comments
    admin.add_view(UsersModelView(Users, db.session))
    admin.add_view(UsersInfoModelView(UserInfo, db.session))
    admin.add_view(ArticlesModelView(Articles, db.session))
    admin.add_view(TagsModelView(Tags, db.session))
    admin.add_view(UserEventModelView(UserEvent, db.session))
    admin.add_view(CommentsModelView(Comments, db.session))
