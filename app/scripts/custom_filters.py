import os
from os.path import dirname, abspath
from ..blog import blog
from datetime import datetime, timedelta
from .main_func import remove_html


@blog.app_template_filter()
def get_first_dir_file(a_id):
    pt = dirname(dirname(abspath(__file__)))
    try:
        needle_path = os.path.join(pt, f'static{os.sep}img{os.sep}{a_id}')
        if needle_path:
            return os.listdir(needle_path)[0]
    except:
        print(f'There is no next folder {needle_path}')
    return ''


@blog.app_template_filter()
def get_first_dir_file_users(a_id):
    pt = dirname(dirname(abspath(__file__)))
    try:
        needle_path = os.path.join(pt, f'static{os.sep}img{os.sep}users{os.sep}{a_id}')
        if needle_path:
            return os.listdir(needle_path)[0]
    except:
        print(f'There is no next folder {needle_path}')
    return ''


@blog.app_template_filter()
def image_exists(dpath):
    app_path = dirname(dirname(abspath(__file__)))
    f_path = os.path.join(app_path, 'static', 'img', dpath)
    if not os.path.isdir(f_path) and os.path.exists(f_path):
        return True
    return False


@blog.app_template_filter()
def article_date_format(adate):
    today = datetime.today()
    if today.date() == adate.date():
        return f"today at {adate.strftime('%H:%M')}"
    elif adate.date() == (today.date()-timedelta(days=1)):
        return f"yesterday at {adate.strftime('%H:%M')}"
    elif today.year == adate.year:
        return adate.strftime('%d %b')
    else:
        return adate.strftime('%d %b %Y')


@blog.app_template_filter()
def cut_article_for_prev(text):
    text = remove_html(text)
    if len(text) > 200:
        preview_text = text[:200]
        last_space = preview_text.rfind(' ')
        if last_space:
            return preview_text[:last_space] + '...'
        return preview_text + '...'
    return text
