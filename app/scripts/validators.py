import re

def check_tags(tags_str):
    if tags_str:
        return bool(re.match('^[A-Za-z0-9_, -]*$', tags_str))
    return False

