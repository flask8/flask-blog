import os
import json
from os.path import dirname, abspath, join, sep
import re
import shutil
import requests as r
from sqlalchemy import func
from flask_login import current_user
from app import db
from ..models import Users, Articles, UserEvent, PostRating


def add_user_event(event_title, event_desc, c_user):
    """Save new user event(login, register) to db
    Args:
        event_title (str): title of event
        event_desc (str): event description
        c_user (str): current user
    Returns:
        None
    """
    user = db.session.query(Users).filter_by(id=c_user.id).first()
    if user:
        new_event = UserEvent(event_title=event_title, event_desc=event_desc)
        user.user_events.append(new_event)
        db.session.commit()


def get_user_location():
    """Get user's location and provider data
    Returns:
        str: on success - location and provider data
        str: on failure - None
    """
    result = ''
    try:
        ip_geoloc_url = 'https://api.ipgeolocation.io/ipgeo?apiKey='
        key = os.getenv('IPGEOLOC_API_KEY')
        response = r.get(ip_geoloc_url + key).text
        data = json.loads(response)
        result = 'from {ip} ({org}) ({country}, {district}, {city})'.format(
           ip=data['ip'], org=data['organization'], country=data['country_name'], district=data['district'],
           city=data['city']
        )
    except:
        print('err')
    return result


def check_img_dir(img_path=f'{os.sep}static{os.sep}img'):
    """Clear directory if exists. If directory doesn't exist, it will be created.
    Args:
        img_path (str): path to static image directory
    Return:
        None
    """
    if os.path.isdir(img_path):
        shutil.rmtree(img_path)
    if not os.path.exists(img_path):
        if not os.path.isdir(img_path):
            os.makedirs(img_path)


def prepare_tags(tags_string):
    """Prepare tags before article creating
    Args:
        tags_string (str):
    Returns:
        list: list of article tags
    """
    tags = tags_string.split(',')
    return [tag.strip() for tag in tags]


def remove_html(text):
    """Removes html tags from article body for article preview
    Args:
        text (str):
    Returns:
        str: text without html tags
    """
    tags = re.compile('<.*?>')
    cleartext = re.sub(tags, '', text)
    cleartext = cleartext.replace('&nbsp;&nbsp;', ' ')
    return cleartext


def email_existing(email):
    """Checks email existing
    Args:
        email (str):  email for checking
    Returns:
        bool: True if exists else False
    """
    return bool(db.session.query(Users).filter_by(email=email).first())


def username_existing(username):
    """Checks username existing
    Args:
        username (str): username for checking
    Returns:
        bool: True if exists else False
    """
    return bool(db.session.query(Users).filter_by(name=username).first())


def check_email(email):
    """Check email format
    Args:
        email (str): email for checking
    Returns:
        bool: True if email match to regex else False
    """
    regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    return True if re.fullmatch(regex, email) else False


def check_name(name):
    """Checks username format
    Args:
        name (str): username for checking
    Returns:
        bool: True if name match to regex else False
    """
    regex = '^[a-zA-Z]{2,40}([0-9]{0,10})$'
    return True if re.fullmatch(regex, name) else False


def title_existing(title):
    """Checks title existing
    Args:
        title (str): article title for checking
    Returns:
        bool: True if title exists else False
    """
    return bool(db.session.query(Articles).filter_by(title=title).first())


"""def pimg_dir_existing(full_path):
    if not os.path.isdir(full_path):
        os.makedirs(full_path)"""


def get_profile_pic_path(user_id, pic_extension):
    """Get path of profile picture
    Args:
        user_id (str): user id
        pic_extension (str): extension of profile picture
    Returns:
         str: full path to profile user picture
    """
    static_part = f'static{sep}img{sep}users{sep}{user_id}'
    abs_to_app = dirname(dirname(abspath(__file__)))
    full_path = join(abs_to_app, static_part)
    check_img_dir(full_path)
    return join(abs_to_app, static_part, str(user_id)+pic_extension)


def prepare_article_rating():
    """Prepare article rating
    Returns:
        tuple: 2 dict with article rating info
    """
    rating_dict = dict()
    check_user_vote = dict()
    if current_user.is_authenticated:
        user_votes = db.session.query(PostRating.post_id, PostRating.user_id, PostRating.rating). \
            filter(PostRating.user_id == current_user.id).all()
        for i in user_votes:
            check_user_vote[i[0]] = i[1], i[2]
    rating_sum = db.session.query(PostRating.post_id, func.sum(PostRating.rating)).group_by(PostRating.post_id).all()
    for rating in rating_sum:
        rating_dict[rating[0]] = rating[1]
    return check_user_vote, rating_dict
