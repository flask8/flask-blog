import sys
from os.path import dirname, abspath
from threading import Thread
from itsdangerous import URLSafeTimedSerializer
from flask import current_app, url_for, render_template, flash
from flask_mail import Message
from app import mail

wsgi_path = dirname(dirname(dirname(abspath(__file__))))
sys.path.insert(0, wsgi_path)

import wsgi


def generate_confirmation_token(email):
    """Generate token for email confirmation
    Args:
        email (str): email to which password change instructions for password will be sent
    Returns:
        str:
    """
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=current_app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token, expiration=300):
    """ Load email from token and check if it still valid
    Args:
        token (str): serialized string
        expiration (int): lifetime of email confirmation token in seconds
    Returns:
        str: on success - dumped email
        bool: on failure - False
    """
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=current_app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except:
        return False
    return email


def send_async_email(msg):
    """Send an asynchronous email
    Args:
        msg (flask_mail.Message):
    Returns:
        None
    """
    with wsgi.app.app_context():
        try:
            mail.send(msg)
        except:
            flash('Something went wrong! Try this operation later.', 'error')


def send_email(to, subject, template):
    """Prepare message obj and create new thread
    Args:
        to (str): email recipients
        subject (str): email subject
        template (str): html template of email
    Returns:
        None
    """
    msg = Message(subject, recipients=[to], html=template, sender=current_app.config['MAIL_DEFAULT_SENDER'])
    thr = Thread(target=send_async_email, args=[msg])
    thr.start()


def send_confirm_letter(email, subject='Please confirm your email!'):
    """Prepare message for email confirmation
    Args:
        email (str): email, which will receive the message
        subject (str): email subject
    Returns:
        None
    """
    token = generate_confirmation_token(email)
    confirm_url = url_for('auth.confirm_email', token=token, _external=True)
    html = render_template('auth/activate_msg.html', confirm_url=confirm_url)
    send_email(email, subject, html)


def send_password_change_btn(email, subject='Password change instruction was sent on your email'):
    """Prepare message for password recovery
    Args:
        email (str): email, which will receive the message
        subject (str): email subject
    Returns:
        None
    """
    token = generate_confirmation_token(email)
    confirm_password_change = url_for('auth.password_change', token=token, _external=True)
    html = render_template('auth/password_msg.html', confirm_password_change=confirm_password_change)
    send_email(email, subject, html)
