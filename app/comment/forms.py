from flask_wtf import FlaskForm
from wtforms import SubmitField, HiddenField
from wtforms.validators import Length
from flask_ckeditor import CKEditorField


class LeaveCommentForm(FlaskForm):
    comment_content = CKEditorField('Leave your comment', validators=[Length(min=1, max=200)])
    pu_id = HiddenField('Hidden field')
    submit = SubmitField('Send')


class EditCommentForm(FlaskForm):
    hidden_com_id = HiddenField('edit-comment')
    submit = SubmitField('Update')


class DeleteCommentForm(FlaskForm):
    hidden_com_id = HiddenField('delete-comment')
    submit = SubmitField('Delete')
