from . import comment
from flask import flash, redirect, url_for, jsonify
from flask_login import login_required, current_user
from .forms import LeaveCommentForm, EditCommentForm, DeleteCommentForm
from app.models import Articles, Comments
from app import db


@comment.route('/leave_comment/<int:post_id>', methods=['POST'])
@login_required
def leave_comment(post_id=0):
    mform = LeaveCommentForm()
    comment_content = mform.comment_content.data
    if not comment_content:
        flash('Comment can not be empty!', 'error')
    else:
        if len(comment_content) > 200:
            flash('Max comment length is 200 symbols!', 'error')
        else:
            article = db.session.query(Articles).filter_by(id=post_id).first()
            if article:
                new_comment = Comments(comment_text=comment_content, author=current_user.id, post_id=post_id)
                db.session.add(new_comment)
                db.session.commit()
                flash('Comment was added!', 'success')
            else:
                flash('Article does not exists!', 'error')
    return redirect(url_for('blog.show_article', article_id=post_id))


@comment.route('/edit_comment/<int:post_id>/', methods=['POST'])
@login_required
def edit_comment(post_id=0):
    edit_comment_form = EditCommentForm()
    response = 'success'
    if edit_comment_form.validate_on_submit():
        comment_id = edit_comment_form.hidden_com_id.data
        comment_id, body = comment_id.split('|', 1)
        comment_id = comment_id.strip()

        if comment_id:
            if db.session.query(Comments).filter_by(id=comment_id).first():
                db.session.query(Comments).filter_by(id=comment_id).update({'comment_text': body})
                db.session.commit()
            else:
                response = 'There is no comment which you want to edit!'
        else:
            response = 'Unknown error!'
    return jsonify({'msg': response})


@comment.route('/delete_comment', methods=['POST'])
@login_required
def delete_comment():
    msg = dict()
    del_form = DeleteCommentForm()
    comment_id = del_form.hidden_com_id.data
    if del_form.validate_on_submit():
        current_comment = db.session.query(Comments).filter_by(id=comment_id).first()
        if current_user.id == current_comment.author or current_user.info.admin:
            db.session.delete(current_comment)
            db.session.commit()
            msg = {'msg': 'Comment was deleted successfully!', 'status': 'success'}
        else:
            msg = {'msg': 'Error', 'status': 'error'}
            flash('You have no permissions to delete this comment!', 'error')
    else:
        msg = {'msg': 'error', 'status': 'error'}
    return jsonify(msg)



