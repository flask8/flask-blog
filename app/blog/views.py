import os
from os.path import dirname, abspath, join
import shutil
from flask import render_template, redirect, request, jsonify, flash, url_for, session, abort, Markup
from flask_login import current_user, login_required
from werkzeug.security import check_password_hash, generate_password_hash
from . import blog
from app import db
from app.scripts.confirm_email import send_confirm_letter
from app.scripts.validators import check_tags
from app.scripts.main_func import prepare_tags, check_img_dir, email_existing, check_email, check_name, \
    username_existing, title_existing, get_profile_pic_path, prepare_article_rating
from app.scripts.custom_filters import get_first_dir_file, article_date_format, cut_article_for_prev, \
    get_first_dir_file_users, image_exists
from .forms import CreateArticle, SearchForm, DeleteForm, UpdateUserSettingsForm, UpdateArticleForm
from app.comment.forms import LeaveCommentForm, EditCommentForm, DeleteCommentForm
from app.models import Tags, Articles, Users, UserInfo, Comments, UserEvent, PostRating
from werkzeug.utils import secure_filename
from sqlalchemy import and_, func


@blog.route('/', methods=['GET', 'POST'])
@blog.route('/index', methods=['GET', 'POST'])
@blog.route('/index/<int:page>', methods=['GET', 'POST'])
def index(page=1):

    search_form = SearchForm()
    delete_form = DeleteForm()
    check_user_vote, rating_dict = prepare_article_rating()
    articles = db.session.query(Articles).join(Users).order_by(db.desc(Articles.on_create)).\
        paginate(page=page, per_page=8, error_out=False)
    if current_user.is_authenticated:
        if not current_user.info.confirmed:
            return redirect(url_for('auth.unconfirmed'))
    return render_template('blog/main.html', articles=articles, search=search_form, delete_form=delete_form,
                           url='blog.index', rating=rating_dict, check_uv=check_user_vote)


@blog.route('/create', methods=['POST', 'GET'])
@login_required
def create_article():
    if not current_user.info.confirmed:
        return redirect(url_for('auth.unconfirmed'))
    form = CreateArticle()
    if form.validate_on_submit():
        title = form.title.data
        art_body = form.article_body.data
        tags = form.tags.data
        if not check_tags(tags):
            flash('Tag field can\'t be empty or you are using forbidden symbols in tag field!', 'warning')
            return render_template('blog/create_article.html', create_form=form)
        tags = prepare_tags(tags)
        tag_obj = []
        for tag in tags:
            if not bool(db.session.query(Tags.tag_name).filter_by(tag_name=tag).first()):
                new_tag = Tags(tag_name=tag)
                db.session.add(new_tag)
                tag_obj.append(new_tag)
            else:
                t = db.session.query(Tags).filter_by(tag_name=tag).first()
                tag_obj.append(t)
        db.session.commit()
        if title and art_body:
            if Articles.query.filter(Articles.title == title).first():
                flash('This title already exists', 'error')
                return render_template('blog/create_article.html', create_form=form)
            new_article = Articles(title=title, article_body=art_body, author=current_user.id)
            for t in tag_obj:
                new_article.tags.append(t)
            db.session.add(new_article)
            db.session.flush()
            f = form.article_picture.data
            if f:
                article_id = new_article.id
                f_extension = '.' + f.filename.rsplit('.', 1)[1]
                upload_img_dir = join(dirname(dirname(abspath(__file__))), f'static{os.sep}img{os.sep}{article_id}{os.sep}')
                check_img_dir(upload_img_dir)
                f.save(os.path.join(upload_img_dir, secure_filename(str(new_article.id) + f_extension)))
            else:
                flash('Choose article image!', 'warning')
                return render_template('blog/create_article.html', create_form=form)
            u_info = db.session.query(UserInfo).filter_by(user_id=current_user.id).first()
            db.session.query(UserInfo).filter(UserInfo.user_id == current_user.id).\
                update({'articles_count': u_info.articles_count + 1})
            db.session.commit()
        flash('Article was added!', 'success')
        return redirect(url_for('blog.index'))
    return render_template('blog/create_article.html', create_form=form)


@blog.route('/update/<int:art_id>', methods=['POST', 'GET'])
def update_article(art_id=0):
    update_article_form = UpdateArticleForm()
    article = db.session.query(Articles).join(Tags, Articles.tags).filter(Articles.id == art_id).first()
    if article.author != current_user.id:
        flash('You have no permissions to update someone else\'s article', 'warning')
        return redirect(url_for('blog.show_article', article_id=art_id))
    if update_article_form.validate_on_submit():
        title = update_article_form.title.data
        article_body = update_article_form.article_body.data
        picture = update_article_form.article_picture.data
        tags = update_article_form.tags.data
        if title:
            if not title_existing(title) or title == article.title:
                if title != article.title:
                    article.title = title
            else:
                flash('This title already exists', 'warning')
        else:
            flash('All fields must be completed!', 'warnings')
        if article_body:
            if article_body != article.article_body:
                article.article_body = article_body
        else:
            flash('All fields must be completed!', 'warning')
        tags_for_article = []
        if tags:
            tags = prepare_tags(tags)
            for tag in tags:
                if not db.session.query(Tags.tag_name).filter_by(tag_name=tag).first():
                    new_tag = Tags(tag_name=tag)
                    db.session.add(new_tag)
                    tags_for_article.append(new_tag)
                else:
                    existing_tag = db.session.query(Tags).filter_by(tag_name=tag).first()
                    tags_for_article.append(existing_tag)
            try:
                article.tags = []
                article.tags = tags_for_article
            except:
                pass
            db.session.commit()
        if picture:
                f_extension = '.' + picture.filename.rsplit('.', 1)[1]
                upload_img_dir = join(dirname(dirname(abspath(__file__))),
                                      f'static{os.sep}img{os.sep}{article.id}{os.sep}')
                check_img_dir(upload_img_dir)
                picture.save(os.path.join(upload_img_dir, secure_filename(str(article.id) + f_extension)))
        else:
            flash('All fields must be completed!', 'warning')
        return redirect(url_for('blog.show_article', article_id=article.id))
    update_article_form.title.data = article.title
    update_article_form.article_body.data = article.article_body
    return render_template('blog/update.html', update_form=update_article_form, article=article, tags=article.tags)


@blog.route('/delete', methods=['POST', 'GET'])
def delete_article():
    form = DeleteForm()
    if request.method == 'GET':
        return abort(404)
    if form.validate_on_submit():
        article_id = form.hidden_article_id.data
        if article_id:
            delete_val = db.session.query(Articles).filter_by(id=article_id).first()
            del_article = db.session.query(Articles, Users.id).join(Users).filter(Articles.id == article_id).first()
            user_info = db.session.query(UserInfo).filter_by(user_id=del_article[1]).first()
            db.session.query(UserInfo).filter_by(user_id=del_article[1]).update({'articles_count': user_info.articles_count - 1})
            db.session.delete(delete_val)
            db.session.commit()
            article_img_path = (dirname(dirname(abspath(__file__))) + f'{os.sep}static{os.sep}img{os.sep}{article_id}')
            if os.path.isdir(article_img_path):
                shutil.rmtree(article_img_path)
    return jsonify({'success': 'sc'})


@blog.route('/tag_search', methods=['POST', 'GET'])
@login_required
def tag_search():
    tag_search = request.args.get('q')
    query = db.session.query(Tags.tag_name).filter(Tags.tag_name.like('%' + (str(tag_search)).lower() + '%'))
    tags = [tag[0] for tag in query.all()]
    return jsonify({'matching_results': tags})


@blog.route('/search/<int:page>', methods=['POST', 'GET'])
def search(page=1):
    form = SearchForm()
    delete_form = DeleteForm()
    check_user_vote, rating_dict = prepare_article_rating()
    if 'search' in session and request.method != 'POST':
        search_data = session['search']
    elif request.method == 'POST':
        search_data = form.searched.data
        session['search'] = search_data
    else:
        search_data = ''
    articles = db.session.query(Articles).join(Tags, Articles.tags).filter(
            (Tags.tag_name == search_data) |
            Articles.title.contains(search_data) |
            Articles.article_body.contains(search_data)
        ).order_by(db.desc(Articles.on_create)).paginate(page=page, per_page=8, error_out=False)

    return render_template('blog/search.html', form=form, articles=articles,  url='blog.search',
                           delete_form=delete_form, rating=rating_dict, check_uv=check_user_vote)


@blog.route('/article/<int:article_id>')
@blog.route('/article')
def show_article(article_id=1):
    leave_comment_form = LeaveCommentForm()
    edit_comment_form = EditCommentForm()
    delete_comment_form = DeleteCommentForm()
    if current_user.is_authenticated:
        if not current_user.info.confirmed:
            return redirect(url_for('auth.unconfirmed'))
    comments = db.session.query(Comments).join(Users).filter(Comments.post_id == article_id).all()
    try:
        article = db.session.query(Articles).join(Users).filter(Articles.id == article_id).first()

        if article is not None:
            return render_template('blog/article.html', article=article, leave_comment_form=leave_comment_form,
                                   edit_comment_form=edit_comment_form,
                                   comments=comments, delete_comment_form=delete_comment_form)
    except:
        print('Page not found')
    return abort(404)


@blog.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    update_form = UpdateUserSettingsForm()
    user_events = db.session.query(UserEvent).filter_by(user_id=current_user.id).order_by(db.desc(UserEvent.on_create)).limit(10).all()
    if update_form.validate_on_submit():
        profile_picture = update_form.profile_picture.data
        old_pass = update_form.current_password.data
        new_pass = update_form.new_password.data
        new_pass_confirm = update_form.confirm_new_password.data
        user = db.session.query(Users).filter_by(id=current_user.id).first()
        flash_success_msg = ''
        if profile_picture:
            pic_extension = '.' + profile_picture.filename.rsplit('.', 1)[1]
            full_pic_path = get_profile_pic_path(current_user.id,  pic_extension)
            profile_picture.save(full_pic_path)
            flash_success_msg += '<p>Profile picture was changed!</p>'
        if old_pass:
            if check_password_hash(current_user.password, old_pass):
                if new_pass and new_pass_confirm:
                    if new_pass == new_pass_confirm:
                        user.password = generate_password_hash(new_pass)
                        flash_success_msg += '<p>Password was changed!</p>'
                    else:
                        flash('New password and confirm password not equal!', 'warning')
                else:
                    flash('To change password you must fill all password fields', 'warning')
            else:
                flash('Current password is wrong!', 'warninug')
        new_email = update_form.change_email.data
        if new_email and new_email != current_user.email:
            if not email_existing(new_email):
                if check_email(new_email):
                    user.email = new_email
                    flash_success_msg += '<p>Email was changed!</p>'
                    db.session.query(UserInfo).filter_by(user_id=current_user.id).update({'confirmed': False})
                    send_confirm_letter(new_email)
                else:
                    flash('Wrong email format!', 'warning')
            else:
                flash('This email already exists.', 'warning')
        new_name = update_form.change_name.data
        if new_name and new_name != user.name:
            if check_name(new_name):
                if not username_existing(new_name):
                    user.name = new_name
                    flash_success_msg += '<p>Username was changed!</p>'
                else:
                    flash('This username already exists', 'warning')
            else:
                flash(Markup('''<p>Wrong username format!</p><p>User name must start with 2 letters.<p>
                             <p>Examples : dino0099, alex, lw2000</p>'''), 'warning')
        db.session.commit()
        if flash_success_msg:
            flash(Markup(flash_success_msg), 'success')
        return redirect(url_for('blog.settings'))
    return render_template('blog/settings.html', update_form=update_form, user_events=user_events)


@blog.route('/users/', methods=['GET'])
@blog.route('/users/<int:user_id>/', methods=['GET'])
def user_profile(user_id=0):
    if not user_id or user_id <= 0:
        return redirect(url_for('blog.index'))
    user = db.session.query(Users).join(UserInfo).filter(Users.id == user_id).first()
    articles_count = db.session.query(Articles).filter_by(author=user_id).count()
    comments_count = db.session.query(Comments).filter_by(author=user_id).count()
    tags = db.session.query(Articles).join(Tags, Articles.tags).filter(Articles.author == user_id).all()
    tag_list = []
    for i in tags:
        tag_list.extend(i.tags)
    return render_template('blog/user_profile.html', user=user, user_id=user_id, articles_count=articles_count,
                           comments_count=comments_count, tags=set(tag_list))


@blog.route('/users/<int:user_id>/articles', methods=['GET'])
@blog.route('/users/<int:user_id>/articles/<int:page>', methods=['GET'])
def user_articles(user_id=0, page=0):
    check_user_vote, rating_dict = prepare_article_rating()
    delete_form = DeleteForm()
    articles = db.session.query(Articles).filter_by(author=user_id).order_by(db.desc(Articles.on_create)).paginate(
        page=page, per_page=8, error_out=False)
    articles_count = db.session.query(Articles).filter_by(author=user_id).count()
    comments_count = db.session.query(Comments).filter_by(author=user_id).count()

    return render_template('blog/user_articles.html', data=articles, user_id=user_id, delete_form=delete_form,
                           url='blog.user_articles', articles_count=articles_count, comments_count=comments_count,
                           rating=rating_dict, check_uv=check_user_vote)


@blog.route('/users/<int:user_id>/comments', methods=['GET'])
@blog.route('/users/<int:user_id>/comments/<int:page>', methods=['GET'])
def user_comments(user_id=0, page=0):
    delete_comment_form = DeleteCommentForm()
    edit_comment_form = EditCommentForm()
    comments = db.session.query(Comments).join(Users).filter(Comments.author == user_id).\
        order_by(db.desc(Comments.on_update)).paginate(page=page, per_page=12, error_out=False)
    articles_count = db.session.query(Articles).filter_by(author=user_id).count()
    comments_count = db.session.query(Comments).filter_by(author=user_id).count()
    return render_template('blog/user_comments.html', data=comments, user_id=user_id, articles_count=articles_count,
                           comments_count=comments_count, url='blog.user_comments',
                           delete_comment_form=delete_comment_form, edit_comment_form=edit_comment_form)


@blog.context_processor
def base():
    form = SearchForm()
    return dict(form=form, os=os)


@blog.errorhandler(404)
def page_not_found(e):
    return render_template('errors/404.html'), 404


@blog.route('/vote', methods=['POST'])
@login_required
def vote():
    pid, btn_val = request.values['data'].split('|')
    estimate = 1 if btn_val == 'up' else -1
    if db.session.query(PostRating).filter(and_(PostRating.user_id == current_user.id,
                                                PostRating.post_id == pid)).first() is not None:
        db.session.query(PostRating).filter(and_(PostRating.user_id == current_user.id,
                                                 PostRating.post_id == pid)).update({'rating': estimate})
    else:
        pr = PostRating(post_id=pid, user_id=current_user.id, rating=estimate)
        db.session.add(pr)
    db.session.commit()
    post_rating = db.session.query(func.sum(PostRating.rating)).filter(PostRating.post_id == pid).\
        group_by(PostRating.post_id).all()
    return jsonify({'rating': post_rating[0][0]})
