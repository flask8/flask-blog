from flask_wtf import FlaskForm
from wtforms import PasswordField, SubmitField, FileField, StringField, HiddenField, EmailField, ValidationError
from wtforms.validators import DataRequired, Length
from flask_wtf.file import FileAllowed
from flask_ckeditor import CKEditorField
from app.models import Articles


class CreateArticle(FlaskForm):
    title = StringField('Enter title', validators=[DataRequired(), Length(1, 100)])
    article_body = CKEditorField('Enter article text', validators=[DataRequired()])
    tags = HiddenField('tags')
    article_picture = FileField('Add article picture', validators=[FileAllowed(['jpg', 'png', 'jpeg'])])
    submit = SubmitField('Add')


class SearchForm(FlaskForm):
    searched = StringField('Enter something')
    submit = SubmitField('Search')


class DeleteForm(FlaskForm):
    hidden_article_id = HiddenField('delete-post')
    delete = SubmitField('Delete post')


class UpdateUserSettingsForm(FlaskForm):
    profile_picture = FileField('Add profile picture', validators=[FileAllowed(['jpg', 'png', 'jpeg'], 'Images only!')])
    current_password = PasswordField('Current password')
    new_password = PasswordField('New password')
    confirm_new_password = PasswordField('Confirm new password')
    change_email = EmailField('Email')
    change_name = StringField('Username')
    submit = SubmitField('Save')


class UpdateArticleForm(CreateArticle):
    article_picture = FileField('Add new article picture', validators=[FileAllowed(['jpg', 'png', 'jpeg'],
                                                                                   'Images only!')])
    submit = SubmitField('Update')
